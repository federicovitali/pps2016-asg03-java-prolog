package u07asg;

import java.util.List;
import java.util.Optional;

enum Player {
    PlayerX,
    PlayerO;

    private int numberOfWin = 0;

    public int getNumberOfWin() {return this.numberOfWin;}

    public void setWin(int updatedWin){this.numberOfWin = updatedWin;}
}

public interface TicTacToe {

    void createBoard();

    List<Optional<Player>> getBoard();

    boolean checkCompleted();

    Optional<Player> checkVictory(boolean isRevengeGame);

    int stupidAImove (Player player);

    int drunkAImove(Player player);

    boolean move(Player player, int i, int j);

    int winCount(Player current, Player winner);

    int incrementWin(int value);
}
