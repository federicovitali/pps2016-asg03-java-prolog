package u07asg;

public class TicTacToeApp {

    public static void main(String[] args){
        try {
            new TicTacToeView(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
            e.printStackTrace();
        }
    }
}