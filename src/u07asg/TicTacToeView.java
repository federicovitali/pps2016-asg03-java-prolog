package u07asg;

import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/**
 * Created by Federico on 27/04/2017.
 */
public class TicTacToeView {

    private final TicTacToe ttt;
    private final JButton[][] board = new JButton[4][4];
    private final JButton exit = new JButton("Exit");
    private final JButton newGame = new JButton("New Game!");
    private final JFrame gameFrame = new JFrame("TTT");
    private final JFrame initialFrame = new JFrame("Game Options");
    private final JLabel infoLabel = new JLabel("Turn of ");
    private final JLabel winLabel = new JLabel("N. of Win: ");
    private final JRadioButton vsSPlayer = new JRadioButton("Second Player");
    private final JRadioButton stupidAI = new JRadioButton("Stupid AI");
    private final JRadioButton drunkAI = new JRadioButton("Drunk AI");
    private final JRadioButton classicTTT = new JRadioButton("Classic TicTacToe");
    private final JRadioButton revengeTTT = new JRadioButton("Revenge TicTacToe");
    private boolean finished,revengeGame,challengerIsStupid,challengerIsDrunk = false;
    private Player turn = Player.PlayerX;
    private int moves = 0;

    TicTacToeView(TicTacToe ttt) throws Exception {
        this.ttt=ttt;
        initStartingOptionsPane();
    }

    private void humanMove(int i, int j){
        if (ttt.move(turn,i,j)){
            board[i][j].setText(turn == Player.PlayerX ? "X" : "O");
            board[i][j].setEnabled(false);
            moves++;
            changeTurn();
            if (moves > 10){
                final int posX = ttt.winCount(turn, Player.PlayerX);
                final int posO = ttt.winCount(turn, Player.PlayerO);
                double winChancesX = Math.round(100.0 / (posX+posO) * posX);
                double winChancesO = Math.round(100.0 - winChancesX);
                if(posO == 0 && posX ==0){
                    winChancesO =0;
                    winChancesX=0;
                }
                System.out.println("Winning probabilities of player X are: "+winChancesX + "%");
                System.out.println("Winning probabilities of player O are: "+winChancesO + "%");
            }

            checkVictory();

            if(challengerIsDrunk || challengerIsStupid) {
                AIMove();
            }
        }
    }

    private void AIMove(){
        if (!finished) {
            int pos;
            if(challengerIsStupid) pos = ttt.stupidAImove(turn);
            else pos = ttt.drunkAImove(turn);
            moves++;
            int j = pos / 4;
            int i = pos % 4;
            board[i][j].setText(turn == Player.PlayerX ? "X" : "O");
            board[i][j].setEnabled(false);
            changeTurn();
        }
        checkVictory();
    }

    private void checkVictory(){
        Optional<Player> victory = ttt.checkVictory(this.revengeGame);
        if (victory.isPresent()){
            victory.get().setWin(ttt.incrementWin(victory.get().getNumberOfWin()));
            infoLabel.setText(victory.get()+" won!");
            winLabel.setText("N. of Win (X|O): "+ Player.PlayerX.getNumberOfWin()+" | "+Player.PlayerO.getNumberOfWin());
            newGame.setEnabled(true);
            finished=true;
            turn = victory.get().equals(Player.PlayerX) ? Player.PlayerO : Player.PlayerX;
            return;
        }
        if (ttt.checkCompleted()){
            infoLabel.setText("Even!");
            newGame.setEnabled(true);
            finished=true;
        }
    }

    private void initGamePane(){
        gameFrame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(4,4));
        for (int i=0;i<4;i++){
            for (int j=0;j<4;j++){
                final int i2 = i;
                final int j2 = j;
                board[i][j]=new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> { if (!finished) humanMove(i2,j2); });
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        newGame.setEnabled(false);
        s.add(newGame);
        s.add(exit);
        JPanel n=new JPanel(new FlowLayout());
        this.infoLabel.setText("Turn of " + turn.name());
        this.winLabel.setText("N. of Win (X|O): "+ Player.PlayerX.getNumberOfWin()+" | "+Player.PlayerO.getNumberOfWin());
        n.add(infoLabel);
        n.add(winLabel);
        exit.addActionListener(e -> System.exit(0));
        newGame.addActionListener(e -> restartGame());
        gameFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        gameFrame.add(BorderLayout.CENTER,b);
        gameFrame.add(BorderLayout.SOUTH,s);
        gameFrame.add(BorderLayout.NORTH,n);
        gameFrame.setSize(400,460);
        gameFrame.setVisible(true);
        gameFrame.setLocationRelativeTo(null);
    }

    private void initStartingOptionsPane(){
        JLabel vsPText = new JLabel("Play with: ");
        JLabel typeGameText = new JLabel("Type of TTT: ");
        JButton startGame = new JButton("Start Game!");
        ButtonGroup gameEnemy = new ButtonGroup();
        ButtonGroup gameType = new ButtonGroup();
        gameEnemy.add(vsSPlayer);
        gameEnemy.add(stupidAI);
        gameEnemy.add(drunkAI);
        gameType.add(classicTTT);
        gameType.add(revengeTTT);
        initialFrame.add(vsPText);
        initialFrame.add(vsSPlayer);
        initialFrame.add(stupidAI);
        initialFrame.add(drunkAI);
        initialFrame.add(typeGameText);
        initialFrame.add(classicTTT);
        initialFrame.add(revengeTTT);
        initialFrame.setSize(400,150);
        initialFrame.setLayout( new FlowLayout());
        vsSPlayer.setSelected(true);
        classicTTT.setSelected(true);
        initialFrame.setVisible(true);
        initialFrame.add(startGame);
        initialFrame.setLocationRelativeTo(null);
        startGame.addActionListener(e -> {
            if(revengeTTT.isSelected()) revengeGame = true;
            if(stupidAI.isSelected()) challengerIsStupid = true;
            if(drunkAI.isSelected()) challengerIsDrunk = true;
            initialFrame.dispose();
            initGamePane();
        });

    }

    private void restartGame(){
        this.finished = false;
        this.moves = 0;
        for (int i=0;i<4;i++)
            for (int j=0;j<4;j++){
                board[i][j].setText("");
                board[i][j].setEnabled(true);
            }
        ttt.createBoard();
        newGame.setEnabled(false);
        if (turn.equals(Player.PlayerO)&&(challengerIsDrunk || challengerIsStupid)) AIMove();
    }

    private void changeTurn(){
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
        this.infoLabel.setText("Turn of " + turn.name());
    }
}
