% random_fact(Head, Instantiation, Index)
:- dynamic(fact_count/2).

% fact_count(Head, Count)
:- dynamic(random_fact/3).

% symbol(+OnBoard,-OnScreen): symbols for the board and their rendering
symbol(null,'_').
symbol(p1,'X').
symbol(p2,'O').

% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"player 1 wins").
result(p2,"player 2 wins").

utility(p1, 1).
utility(p2, -1).
utility(even, 0).

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% render(+List): prints a TTT table (9 elements list) on console
render(L) :- convert_symbols(L,[A,B,C,D,E,F,G,H,I]),print_row(A,B,C),print_row(D,E,F),print_row(G,H,I).
convert_symbols(L,L2) :- findall(R,(member(X,L),symbol(X,R)),L2).
print_row(A,B,C) :- put(A),put(' '),put(B),put(' '),put(C),nl.

% render(+List,+Result): prints a TTT table plus result
render_full(L,Result) :- result(Result,OnScreen),print(OnScreen),nl,render(L),nl,nl.

% create_board(-Board): creates an initially empty board
create_board(B):-create_list(16,null,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

% next_board(+Board,+Player,?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B],PL,[PL|B]).
next_board([X|B],PL,[X|B2]):-next_board(B,PL,B2).

% final(+Board,-Result): checks where the board is final and why
final(B,p1) :- finalpatt(P), match(B,P,p1),!.
final(B,p2) :- finalpatt(P), match(B,P,p2),!.
final(B,even) :- not(member(null,B)).

% match(Board,Pattern,Player): checks if in the board, the player matches a winning pattern
match([],[],_).
match([M|B],[x|P],M):-match(B,P,M).
match([_|B],[o|P],M):-match(B,P,M).

% finalpatt(+Pattern): gives a winning pattern
finalpatt([x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x]).
finalpatt([x,o,o,o,x,o,o,o,x,o,o,o,x,o,o,o]).
finalpatt([o,x,o,o,o,x,o,o,o,x,o,o,o,x,o,o]).
finalpatt([o,o,x,o,o,o,x,o,o,o,x,o,o,o,x,o]).
finalpatt([o,o,o,x,o,o,o,x,o,o,o,x,o,o,o,x]).
finalpatt([x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x]).
finalpatt([o,o,o,x,o,o,x,o,o,x,o,o,x,o,o,o]).


% game(+Board,+Player,-FinalBoard,-Result): finds one (zero, one or many) final boards and results
game(B,_,B,Result) :- final(B,Result),!.
game(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game(B2,PL2,BF,Result).

% statistics(+Board,+Player,+Result,-Count): counts how many time Res will happen 
statistics(B,P,Res,Count) :- findall(a, game(B,P,_,Res),L), length(L,Count).

% final_revenge(+Board,-Result): check if the board is final and respects the revenge rule
final_revenge(B,p1) :- finalpatt(P), match(B,P,p1),simulate_move_and_check_win(B,p2,p2),!.
final_revenge(B,p2) :- finalpatt(P), match(B,P,p2),simulate_move_and_check_win(B,p1,p1),!.

% replace(+ElementToSub,+ElementToPut,+Board,-NewBoard): replace every ElementToSub by ElementToPut on Board.
replace(_, _, [], []).
replace(O, R, [O|T], [R|T2]) :- replace(O, R, T, T2).
replace(O, R, [H|T], [H|T2]) :- H \= O, replace(O, R, T, T2).

% simulate_move_and_check_win(+Board,+Player,-Result): check the Revenge rule(if the other player can win during his next turn).
simulate_move_and_check_win(B,PL,Result):-
  other_player(PL,PS),
  replace(PS,block,B,BN),
  next_board(BN,PL,B2),
  final(B2,Result),!.

% set_randomization_variable(+Goal): given a goal take all the occurrences and put in variables with a index to numerate them (start to 0).
set_randomization_variable(Goal) :-
  findall(Goal, Goal, Occurrances),
  set_randomization_variable(Occurrances, 0),
  Goal =.. [Head|_],
  length(Occurrances, N),
  asserta(fact_count(Head, N)).

% set_randomization_variable(+Goal,+Index): save in random_fact all the results (in this case the output of new_board) with an incremental index.
set_randomization_variable([], _).
set_randomization_variable([Goal|Goals], N) :-
  Goal =.. [Head,First,Second,Third|_],
  asserta(random_fact(Head, Third, N)),
  N1 is N+1,
  set_randomization_variable(Goals, N1), !.

% get_random_pred(+Head,-Predicate): given predicate s head search in the variables all the occurrences and take one of them randomly.
get_random_pred(Head, Pred) :-
  fact_count(Head, N),
  rand_int(N, I),
  random_fact(Head, Pred, I), !.

%increment(+Val,-Result): increment Val as java ++
increment(Val,Result):- Result is Val+1.